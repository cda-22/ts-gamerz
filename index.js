var express = require('express')
var app = express()

var fileReader = require('fs');

app.use(express.static('static'));

app.get('/', function (req, res) {
    let file = fileReader.readFileSync('index.html',{encoding : 'utf-8'});
   
  res.type('html').send(file);
})

app.listen(3000)
